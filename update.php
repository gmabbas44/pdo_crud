<?php 	include ("_include/header.inc.php"); ?>
<?php

	$crud = new CRUD; // object CRUE class;

	if (isset($_GET['edit_id'])) {
		$edit_id = $_GET['edit_id'];
		$this_data = $crud->select_is_data("oop_insert_test",$edit_id);
		$row = $this_data->fetch(PDO::FETCH_OBJ);
	}else{
		header("location: view.php");
	}
	if (isset($_POST['update'])) {
		$fullname = $_POST['fullname'];
		$username = $_POST['username'];
		$password = $_POST['password'];

		$update_array = array(
						'fullname' => $fullname, 
						'username' => $username, 
						'password' => $password 
						);
		$update = $crud->update_data("oop_insert_test",$update_array,$edit_id);
		if ($update) {
			header("Location: view.php");
		}
	}

 ?>
<div class="card">
	<div class="card-header h2 text-center bg-primary text-white">
		Update data
	</div>
	<div class="card-body">
		<?php 
			if (isset($insertData)) {
				if ($insertData == true) {
			?>
			<div class="alert alert-success">
				<strong>Welcome! </strong>Insert successfully. <a href="view.php">View data</a>
			</div>

			<?php
				}else{
			?>
			<div class="alert alert-success">
				<strong>Oops! </strong>Insert not successfully.
			</div>
			<?php 

				}
			}
		?>
		
		<form action="" method="POST">
			<div class="form-group">
				<label for="full name">Full Name</label>
				<input class="form-control" type="text" name="fullname" id="fullname"  value="<?php echo $row->fullname; ?>" required />
			</div>
			<div class="form-group">
				<label for="user name">User Name</label>
				<input class="form-control" type="text" name="username" id="username"  value="<?php echo $row->username; ?>" required />
			</div>
			<div class="form-group">
				<label for="password">Password</label>
				<input class="form-control" type="password" name="password" id="password"  value="<?php echo $row->password; ?>" required />
			</div>
			<button class="btn btn-info" type="submit" name="update">Update</button>
			<a href="view.php" class="btn btn-primary ml-2">View</a>
		</form>
	</div>
</div>

<?php 	include ("_include/footer.inc.php"); ?>