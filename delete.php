<?php 	include ("_include/header.inc.php"); ?>


<?php 

	$crud = new CRUD;
	// Get data from viwe.php;
	if (isset($_GET['delete_id'])) {
		$user_id = $_GET['delete_id'];

		$this_data = $crud->select_is_data("oop_insert_test",$user_id);
		$row = $this_data->fetch(PDO::FETCH_OBJ);
	}
	else{
		header("Location: view.php");
	}

	// delete data
	if (isset($_POST['delete'])) {
		$delete_id = $_POST['user_id'];
		$delect_user = $crud->delete("oop_insert_test",$user_id);
		if ($delect_user == true) {
			header("location: view.php");
		}
	}

 ?>


<div class="card">
	<div class="card-body">
		<div class="alert alert-danger">
			<strong>Sure!! </strong>delete following record?
		</div>
		<table class="table table-bordered text-center">
			<thead class="thead table-primary">
				<tr>
					<th>ID</th>
					<td>Fullname</th>
					<th>Username</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td><?php echo $row->id; ?></td>
					<td><?php echo $row->fullname; ?></td>
					<td><?php echo $row->username; ?></td>
				</tr>
			</tbody>
		</table>
		<form action="" method="POST">
			<input type="hidden" name="user_id" value="<?php echo $row->id; ?>">
			<td class="text-center"><button class="btn btn-danger py-2" type="submit" name="delete">YES</button></td>
			<a href="view.php" class="btn btn-primary py-2">NO</a>
		</form>
		
	</div>
</div>

<?php 	include ("_include/footer.inc.php"); ?>