<?php 	include ("_include/header.inc.php"); ?>
<?php
	$crud = new CRUD;
	
	if (isset($_POST['insert'])) {
		$fullname = $_POST['fullname'];
		$username = $_POST['username'];
		$password = $_POST['password'];
		if (empty($fullname) || empty($username) || empty($password)) {
			$mgs = "Field must not be empty";
		}else {
			$dataArray =  array(
				'fullname' => $fullname,
				'username' => $username,
				'password' => $password
			);
			$insertData = $crud->insertData("oop_insert_test", $dataArray);
		}
	}

 ?>
<div class="card">
	<div class="card-header h2 text-center bg-primary text-white">
		Insert data
	</div>
	<div class="card-body">
		<?php 
			if (isset($insertData)) {
				if ($insertData == true) {
			?>
			<div class="alert alert-success">
				<strong>Welcome! </strong>Insert successfully. <a href="view.php">View data</a>
			</div>

			<?php
				}else{
			?>
			<div class="alert alert-success">
				<strong>Oops! </strong>Insert not successfully.
			</div>
			<?php 

				}
			}
		?>
		
		<form action="" method="POST">
			<div class="form-group">
				<label for="full name">Full Name</label>
				<input class="form-control" type="text" name="fullname" id="fullname"  value="" required />
			</div>
			<div class="form-group">
				<label for="user name">User Name</label>
				<input class="form-control" type="text" name="username" id="username"  value="" required />
			</div>
			<div class="form-group">
				<label for="password">Password</label>
				<input class="form-control" type="password" name="password" id="password"  value="" required />
			</div>
			<button class="btn btn-info" type="submit" name="insert">Insert</button>
			<a href="view.php" class="btn btn-primary ml-2">View</a>
		</form>
	</div>
</div>

<?php 	include ("_include/footer.inc.php"); ?>