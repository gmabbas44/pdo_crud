<?php 	include ("_include/header.inc.php"); ?>
<?php 
	$crud = new CRUD;
 ?>
<?php 
	/* code for pagination */
	$start = 0;
	$limit = 20;
	//$page = 10;
	if (isset($_GET['page_id'])) {
		$page_id = $_GET['page_id'];
		/* function pagination($tablename, $start, $limit,$page_id)*/
		$result = $crud->pagination("oop_insert_test",$start,$limit,$page_id);
		$start = $result['start'];
		$page = $result['page'];
	}else{
		$page_id = 1;
		$page = 1;
	}

	/* code for pagination end */

 ?>
	<table class="table table-bordered">
	<thead class="thead-light">
		<tr class="text-center">
			<th>Id</th>
			<th>Fullname</th>
			<th>Username</th>
			<th>Password</th>
			<th>Update</th>
			<th>Delete</th>
		</tr>
	</thead>
	<tbody>
		<?php 
			$data = $crud->dataview("oop_insert_test",$start,$limit);
			while ($row = $data->fetch(PDO::FETCH_OBJ)) {
		 ?>
		<tr>
			<td><?php echo $row->id; ?></td>
			<td><?php echo $row->fullname; ?></td>
			<td><?php echo $row->username; ?></td>
			<td><?php echo $row->password; ?></td>
			<td class="text-center"><a class="btn btn-primary" href="update.php?edit_id=<?php echo $row->id; ?>">Update</a></td>
			<td class="text-center"><a class="btn btn-danger" href="delete.php?delete_id=<?php echo $row->id; ?>">Delete</a></td>

		</tr>
		<?php } ?>
	</tbody>
</table>

<nav area-label="Page navigation">
	<ul class="pagination justify-align-center">
		<li class="page-item"><a href="#" class="page-link"><span aria-hidden="true">&laquo;</span><span class="sr-only">Previous</span></a></li>

		<?php for ($i=1; $i <= $page; $i++) { ?>
			<li class="page-item"><a href="<?php echo $_SERVER['PHP_SELF']."?page_id=".$i; ?>" class="page-link"><?php echo $i; ?></a></li>
		<?php } ?>

		<li class="page-item"><a href="#" class="page-link"><span aria-hidden="true">&raquo;</span><span class="sr-only">Next</span></a></li>
	</ul>
</nav>

<?php 	include ("_include/footer.inc.php"); ?>
	

