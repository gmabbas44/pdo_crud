-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 22, 2019 at 06:03 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `oop`
--

-- --------------------------------------------------------

--
-- Table structure for table `info`
--

CREATE TABLE `info` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `roll` varchar(240) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `info`
--

INSERT INTO `info` (`id`, `name`, `roll`) VALUES
(1, 'GM Abbas Uddin', '382633'),
(2, 'GM Abbas Uddin', '382633'),
(3, 'GM Abbas Uddin', '382633'),
(4, 'GM Abbas Uddin', '382633'),
(5, 'GM Abbas Uddin', '382633'),
(6, 'GM Abbas Uddin', '382633'),
(7, 'GM Abbas Uddin', '382633'),
(8, 'GM Abbas Uddin', '382633'),
(9, 'GM Abbas Uddin', '382633');

-- --------------------------------------------------------

--
-- Table structure for table `myguests`
--

CREATE TABLE `myguests` (
  `id` int(6) UNSIGNED NOT NULL,
  `firstname` varchar(30) NOT NULL,
  `lastname` varchar(30) NOT NULL,
  `email` varchar(50) DEFAULT NULL,
  `reg_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `my_db`
--

CREATE TABLE `my_db` (
  `id` int(11) NOT NULL,
  `unic_id` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `roll` varchar(240) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `my_db`
--

INSERT INTO `my_db` (`id`, `unic_id`, `name`, `roll`) VALUES
(1, '', 'GM Abbas Uddin', '382633'),
(2, '', 'GM Abbas Uddin', '382633'),
(3, '', 'GM Abbas Uddin', '382633'),
(4, '', 'GM Abbas Uddin', '382633'),
(5, '', 'GM Abbas Uddin', '382633'),
(6, '', 'GM Abbas Uddin', '382633'),
(7, '', 'GM Abbas Uddin', '382633'),
(8, '', 'GM Abbas Uddin', '382633');

-- --------------------------------------------------------

--
-- Table structure for table `oop_insert_test`
--

CREATE TABLE `oop_insert_test` (
  `id` int(10) NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `oop_insert_test`
--

INSERT INTO `oop_insert_test` (`id`, `fullname`, `username`, `password`) VALUES
(47, 'Md. Akbar Hossin', ' akbar', '1234'),
(48, 'Ali Ahsan', ' Fardin', '1234'),
(51, 'Al Amin', ' Shek', ' 1234'),
(52, 'wahid', ' hasan', '1234'),
(53, 'Md. Akbar Hossin', ' akbar', ' 123'),
(54, 'Md. Akbar Hossin', 'akbar', '123'),
(56, 'Akib', 'jabed', '123'),
(57, 'Md. Abbas Uddin', 'abbas', '123'),
(58, 'Gmaabbas ', 'Abbas', '12'),
(59, 'abc', 'abbas', '1234');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `info`
--
ALTER TABLE `info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `myguests`
--
ALTER TABLE `myguests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `my_db`
--
ALTER TABLE `my_db`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `oop_insert_test`
--
ALTER TABLE `oop_insert_test`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `info`
--
ALTER TABLE `info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `myguests`
--
ALTER TABLE `myguests`
  MODIFY `id` int(6) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `my_db`
--
ALTER TABLE `my_db`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `oop_insert_test`
--
ALTER TABLE `oop_insert_test`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
