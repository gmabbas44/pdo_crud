<?php 	

/**
* data view
*/
class CRUD extends DbConnect
{
	/* 
		function for dataview
	*/
	public function dataview($tableName,$start,$limit)
	{
		$query = "SELECT * FROM $tableName ORDER BY id DESC LIMIT $start,$limit";
		$data = $this->con->prepare($query);
		$data->execute();
		return $data;
		exit();
	}
	/* 
		function for insertData
	*/
	public function insertData($tableName, $dataArray)
	{
		$query = "INSERT INTO $tableName(".implode(",", array_keys($dataArray)).") VALUES('".implode("',' ", array_values($dataArray))."')";
		$insertData = $this->con->prepare($query);
		$insertData->execute();

		if ($insertData) {
			return true;
		}else {
			return false;
		}
		exit();
	}
	/* 
		function for select a specipic data
	*/
	public function select_is_data($tableName,$user_id)
	{
		$query = "SELECT * FROM $tableName WHERE id = :id";
		$this_data = $this->con->prepare($query);
		$this_data->bindparam(":id",$user_id);
		$this_data->execute();
		return $this_data;
		exit();
	}
	/* 
		function for Delete Data
	*/
	public function delete($tableName,$user_id)
	{
		$query = "DELETE FROM $tableName WHERE id = :id";
		$del = $this->con->prepare($query);
		$del->bindparam(":id",$user_id);
		$del->execute();
		if ($del) {
			return true;
		}else {
			return false;
		}
		exit();
	}
	/* 
		function for Update Data
	*/
	public function update_data($tableName,$update_array,$edit_id)
	{
		foreach ($update_array as $key => $value) {
			$v[] = $key."="."'".$value."'";
		}
		$data = implode(",",$v);
		$query = "UPDATE $tableName SET $data WHERE id = :edit_id";
		$update = $this->con->prepare($query);
		$update->execute([':edit_id' => $edit_id]);
		if ($update) {
			return true;
		}else {
			return false;
		}
	}
	/* 
		function for pagination
	*/
	public function pagination($tablename,$start,$limit,$page_id)
	{
		$query ="SELECT * FROM $tablename";
		$data = $this->con->prepare($query);
		$totalRow = $data->rowCount();
		$start = $start;
		$limit = $limit;
		$start = ($page_id - 1) * $limit;
		$page = ceil($totalRow / $limit);
		$result =  array('start' => $start,'page' => $page);
		return $result;
	}

	
}


 ?>