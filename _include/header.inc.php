<?php 	include ("_include/db_config.inc.php"); ?>
<?php 	include ("_include/class.inc.php"); ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>PDO CRUD Operation</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<link rel="stylesheet" href="css/style.css">
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-light bg-warning">
		<div class="container">
			<a href="#" class="navbar-brand">PDO CRUD</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarNav">
				<ul class="navbar-nav ml-auto">
					<li class="nav-item active">
						<a href="insert.php" class="nav-link">Insert</a>
					</li>
					<li class="nav-item">
						<a href="view.php" class="nav-link">View</a>
					</li>
				</ul>
			</div>
		</div>
	</nav>
	<div class="container mt-4">